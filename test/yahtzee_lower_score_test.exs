defmodule YahtzeeLowerScoreTest do
  use ExUnit.Case

  def generate(dice_face, occurrences) do
    Enum.to_list(1..6)
    |> List.delete(dice_face)
    |> Enum.shuffle
    |> Enum.take(5 - occurrences)
    |> Enum.concat(List.duplicate(dice_face, occurrences))
    |> Enum.shuffle
  end

  test "Identify 'Three of a kind' with ones" do
    dices = generate(1, 3)
    sum = Enum.sum(dices)
    assert %{"Three of a kind": ^sum} = Yahtzee.score_lower(dices)
  end

  test "Identify 'Three of a kind' with all the others" do
    Enum.map(2..6, fn (dice_face) ->
      dices = generate(dice_face, 3)
      sum = Enum.sum(dices)
      assert %{"Three of a kind": ^sum} = Yahtzee.score_lower(dices)
    end)
  end

  test "Identify 'Four of a kind' with every face" do
    Enum.map(1..6, fn (dice_face) ->
      dices = generate(dice_face, 4)
      sum = Enum.sum(dices)
      assert %{"Four of a kind": ^sum} = Yahtzee.score_lower(dices)
    end)
  end

  test "Identify 'Full house' with every face" do
    Enum.map(1..6, fn _ ->
      [x,y] =
        Enum.shuffle(1..6)
        |> Enum.take(2)
      assert %{"Full house": 25} = Yahtzee.score_lower([x,x,x,y,y] |> Enum.shuffle)
    end)
  end

  test "Identify 'Small straight' with every face" do

    straightLength = 4
    possibleNumbers = Enum.to_list(1..6)
    shiftIndex = 6 - straightLength
    Enum.map(0..shiftIndex, fn index ->

      first = index + 1
      last = first + (straightLength - 1)
      samllStraightArray = Enum.to_list(first..last)

      lowerBound = first - 1
      upperBound = last + 1
      currentPossibleNumbers = List.delete(possibleNumbers, lowerBound)
      |> List.delete(upperBound)

      result = Enum.take_random(currentPossibleNumbers, 1)
      |> Enum.concat(samllStraightArray)
      |> Enum.shuffle
      |> Yahtzee.score_lower()

      assert %{"Small straights": 30} = result
    end)

  end

  test "Identify 'Large straight' with every face" do

    straightLength = 5
    possibleNumbers = Enum.to_list(1..6)
    shiftIndex = 6 - straightLength
    Enum.map(0..shiftIndex, fn index ->

      first = index + 1
      last = straightLength + index
      result = Enum.to_list(first..last)
      |> Enum.shuffle
      |> Yahtzee.score_lower()

      assert %{"Large straights": 40} = result
    end)

  end

  test "Identify 'Yahtzee'" do

    Enum.map(1..6, fn n ->
      assert %{Yahtzee: 50} = Yahtzee.score_lower(List.duplicate(n,5))
    end)

  end

  test "Identify any other combination" do

    Enum.map(1..6, fn _ ->
      [x,y,z] =
        Enum.shuffle(1..6)
        |> Enum.take(3)
      seq = Enum.shuffle([x,x,y,y,z])
      sum = Enum.sum(seq)
      assert %{Chance: ^sum} = Yahtzee.score_lower(seq)
    end)

  end

  test "verifies the score of the combination - 1" do
    # x, x, y, y, y
    assert Yahtzee.score([1, 3, 1, 3, 3]) == %{
        Ones: 2,
        Twos: 0,
        Threes: 3,
        Fours: 0,
        Fives: 0,
        Sixes: 0,
        "Three of a kind": 11,
        "Four of a kind": 0,
        "Full house": 25,
        "Small straights": 0,
        "Large straights": 0,
        Yahtzee: 0,
        Chance: 0
      }

  end

  test "verifies the score of the combination - 2" do
    # x, x, x, x, y
    assert Yahtzee.score([3, 3, 4, 3, 3]) == %{
        Ones: 0,
        Twos: 0,
        Threes: 4,
        Fours: 1,
        Fives: 0,
        Sixes: 0,
        "Three of a kind": 0,
        "Four of a kind": 16,
        "Full house": 0,
        "Small straights": 0,
        "Large straights": 0,
        Yahtzee: 0,
        Chance: 0
      }

  end

  test "verifies the score of the combination - 3" do
    # x, y, z, z, z
    assert Yahtzee.score([1, 2, 4, 2, 2]) == %{
        Ones: 1,
        Twos: 3,
        Threes: 0,
        Fours: 1,
        Fives: 0,
        Sixes: 0,
        "Three of a kind": 11,
        "Four of a kind": 0,
        "Full house": 0,
        "Small straights": 0,
        "Large straights": 0,
        Yahtzee: 0,
        Chance: 0
      }

  end

  test "verifies the score of the combination - 4" do
    # v, w, x, y, z
    assert Yahtzee.score([2, 3, 4, 5, 6]) == %{
        Ones: 0,
        Twos: 1,
        Threes: 1,
        Fours: 1,
        Fives: 1,
        Sixes: 1,
        "Three of a kind": 0,
        "Four of a kind": 0,
        "Full house": 0,
        "Small straights": 0,
        "Large straights": 40,
        Yahtzee: 0,
        Chance: 0
      }

  end

  test "verifies the score of the combination - 5" do
    # w, w, x, y, z
    assert Yahtzee.score([2, 2, 3, 4, 5]) == %{
        Ones: 0,
        Twos: 2,
        Threes: 1,
        Fours: 1,
        Fives: 1,
        Sixes: 0,
        "Three of a kind": 0,
        "Four of a kind": 0,
        "Full house": 0,
        "Small straights": 30,
        "Large straights": 0,
        Yahtzee: 0,
        Chance: 0
      }

  end

  test "verifies the score of the combination - 6" do
    # x, x, x, x, x
    assert Yahtzee.score([2, 2, 2, 2, 2]) == %{
        Ones: 0,
        Twos: 5,
        Threes: 0,
        Fours: 0,
        Fives: 0,
        Sixes: 0,
        "Three of a kind": 0,
        "Four of a kind": 10,
        "Full house": 0,
        "Small straights": 0,
        "Large straights": 0,
        Yahtzee: 50,
        Chance: 0
      }

  end

end
