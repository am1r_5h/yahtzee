defmodule Yahtzee do

  def score_upper(dice) do
    %{
        Ones: length(Enum.filter(dice, fn e -> e == 1 end)),
        Twos: length(Enum.filter(dice, fn e -> e == 2 end)),
        Threes: length(Enum.filter(dice, fn e -> e == 3 end)),
        Fours: length(Enum.filter(dice, fn e -> e == 4 end)),
        Fives: length(Enum.filter(dice, fn e -> e == 5 end)),
        Sixes: length(Enum.filter(dice, fn e -> e == 6 end))
    }
  end

  def score_lower(dice) do

    result = %{}
    result = getThreeOfAKindResult(dice, result)
    result = getFourOfAKindResult(dice, result)
    result = getFullHouseResult(dice, result)
    result = getSmallStraight(dice, result)
    result = getLargeStraight(dice, result)
    result = getYahtzee(dice, result)
    getChance(dice, result)

  end

  def score(dice) do

    map1 = score_upper(dice)
    map2 = score_lower(dice)
    Map.merge(map1, map2)

  end


  # -------------------------------------------------------- private members

  defp getThreeOfAKindResult(dice, prevResult_map) do

    groups = Enum.group_by(dice, fn x -> x end)
    dicesWithValidLengthForThree = Enum.filter(groups, fn x -> length(elem(x, 1)) <= 3  end)
    is_Three_of_a_kind =
        Enum.count(dicesWithValidLengthForThree) <= 3 and
        Enum.any?(dicesWithValidLengthForThree, fn x -> length(elem(x, 1)) == 3 end)

    Map.merge(prevResult_map, %{"Three of a kind": if is_Three_of_a_kind do Enum.sum(dice) else 0 end})

  end

  defp getFourOfAKindResult(dice, prevResult_map) do

    groups = Enum.group_by(dice, fn x -> x end)
    is_Four_of_a_kind = Enum.any?(groups, fn x -> length(elem(x, 1)) >= 4 end)

    Map.merge(prevResult_map, %{"Four of a kind": if is_Four_of_a_kind do Enum.sum(dice) else 0 end})

  end

  defp getFullHouseResult(dice, prevResult_map) do

    groups = Enum.group_by(dice, fn x -> x end)
    dicesWithValidLengthForHouse = Enum.filter(groups, fn x -> length(elem(x, 1)) == 2 or length(elem(x, 1)) == 3  end)
    is_Full_House = Enum.count(dicesWithValidLengthForHouse) == 2 and Enum.any?(dicesWithValidLengthForHouse,
      fn x -> length(elem(x, 1)) == 3 end)

    Map.merge(prevResult_map, %{"Full house": if is_Full_House do 25 else 0 end})

  end

  defp getSmallStraight(dice, prevResult_map) do

    sortedDice = Enum.sort(dice)
    untilIndex = (length(sortedDice) - 1) - 1
    pairs = Enum.slice(Enum.zip(sortedDice, tl(sortedDice) ++ [0]), 0..untilIndex)
    differencesInPairs = Enum.count(pairs, fn x -> abs(elem(x, 0) - elem(x, 1)) == 1 end)
    is_small_straight = differencesInPairs >= 3 and differencesInPairs != 4

    Map.merge(prevResult_map, %{"Small straights": if is_small_straight do 30 else 0 end})

  end

  defp getLargeStraight(dice, prevResult_map) do

    sortedDice = Enum.sort(dice)
    untilIndex = (length(sortedDice) - 1) - 1
    pairs = Enum.slice(Enum.zip(sortedDice, tl(sortedDice) ++ [0]), 0..untilIndex)
    differencesInPairs = Enum.count(pairs, fn x -> abs(elem(x, 0) - elem(x, 1)) == 1 end)
    is_large_straight = differencesInPairs == 4

    Map.merge(prevResult_map, %{"Large straights": if is_large_straight do 40 else 0 end})

  end

  defp getYahtzee(dice, prevResult_map) do

    Map.merge(prevResult_map, %{"Yahtzee": if length(Enum.dedup(dice)) == 1 do 50 else 0 end})

  end

  defp getChance(dice, prevResult_map) do

    prevSum = Enum.reduce(prevResult_map, 0, fn x, acc -> elem(x, 1) + acc end)
    is_chance = prevSum == 0
    Map.merge(prevResult_map, %{"Chance": if is_chance do Enum.sum(dice) else 0 end})

  end

end
